/**
 * Mocha dependencies
 */
var assert = require('assert')

/**
 * Looper library to test
 */
var Looper = require('./index.js').Looper

/**
 * Test Logic
 */
describe('Looper', function () {
  before(function () {
    this.jsdom = require('jsdom-global')(null, { pretendToBeVisual: true })
  })
  /**
   * Constructor
   */
  describe('#constructor', function () {
    it('should create an instance of the Looper class and return this instance <Object>.', function () {
      assert.equal(typeof new Looper(), 'object')
    })
  })

  var looper = new Looper()

  describe('#start', function () {
    it('should start the loop function, based on requestAnimationFrame and create a unique id by frame.', function () {
      // assert.throws(looper.start)
      looper.start()
      assert.ok(typeof looper.id === 'number')
      assert.ok(typeof looper.startTimestamp === 'number')
      assert.ok(typeof looper.currentTimestamp === 'number')
      assert.ok(typeof looper.elapsedTimestamp === 'number')
      assert.ok(looper.isRunning())
    })
  })

  describe('#stop', function () {
    it('should stop the loop function and clear the frame id and the timestamps', function () {
      // assert.throws(looper.stop)
      looper.stop()
      assert.ok(looper.id === null || looper.id === undefined)
      assert.ok(looper.startTimestamp === null)
      assert.ok(looper.currentTimestamp === null)
      assert.ok(looper.elapsedTimestamp === null)
      assert.ok(looper.isRunning() === false)
    })
  })

  describe('#isRunning', function () {
    it('should return whether or not <Booleen> the loop function is running.', function () {
      assert.ok(looper.isRunning() === true || looper.isRunning() === false)
    })
  })

  describe('#add', function () {
    it('should add a function to the actions<Set> call stack. Argument must be a typeof function.', function () {
      assert.throws(looper.add)
      assert.ok(typeof looper.add(function () {}), 'object')
    })
  })

  describe('#dispose', function () {
    it('should clear the actions<Array>, stop the loop function, and clean the frame id.', function () {
      looper.dispose()
      assert.ok(looper.id === null || looper.id === undefined)
    })
  })
})
