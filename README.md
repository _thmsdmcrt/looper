# Looper

[![js-standard-style][standard-image]][standard-url]
[![NPM version][npm-image]][npm-url]
[![stability][stability-image]][stability-url]

## What is it ?
Looper is a small library to provide a helpfull wrapper around window.requestAnimationFrame. 
It is develop with Ecmascript 2015 module system to be compliant with the next javascript spec. 
You should use a transpiler, like [Babel](https://babeljs.io/) to use it for production.

## How it works ?
### Install
With ```npm```
```bash
npm install --save @thmsdmcrt_/looper
```

With ```yarn```
```bash
yarn add @thmsdmcrt_/looper
```
### Basic Usage

Here is a simple exemple of the use of ```Looper``` class :

```javascript
'use strict'
import { Looper } from '@thmsdmcrt_/looper'

// Create a new instance of Looper.
const looper = new Looper()

// Create a method to call in looper.
function doSomething () {
  console.log('looper is running...')
}

// Add doSomething() to the looper instance. Save it to a variable to remove it later.
const addedDoSomethingMethod = looper.add(doSomething) // { remove : function() {} }
// Remove the doSomething() from loop call stack.
addedDoSomethingMethod.remove()

// Start the looper
looper.start()

// Get the looper status. return booleen
const isLooperRunning = looper.isRunning()

// Stop the looper instance. Here with a setTimeout
looper.stop()

// Clear the looper instance.
looper.dispose()

```
Alternatively, you could set a duration to the loop :

```javascript  
'use strict'
import { Looper } from '@thmsdmcrt_/looper'

const looper = new Looper()

// It will stop automatically after 2000 milliseconds.
// Hooks are optionals and outside the actions calls. 
// They are automatically removed at the end of the loop.

looper.start(2000, {
  onStart () {
    // When the loop start. Called before the first requestAnimationFrame
  },
  onUpdate (progress) {
    // You can get the current progress and do some logic with it when the loop is running. Called each frame.
  },
  onStop () {
    // When the loop finish. Called after the first requestAnimationFrame
  }
})

```
### Debug

The ```Looper``` class come with a debug option. It warns you when the Actions call stack take more than one frame.

```javascript
new Looper({debug: true})

// If the frame is longer than 16ms (based on the window.perfromance API)
> 'The Looper instance callstack actions is to heavy, framerate budget exceeded.'
```

## Testing with [Mocha](https://mochajs.org/)
To run the tests, please execute ```npm test```.

## Build
To build, please run ```npm run build```.

## Test Server
With the help of [Budo](https://github.com/mattdesl/budo), the devDependencies provide a simple test server.
It uses the ```browser.test.js```. Please run ```npm run test:server```.

## Contributing
Please, see [CONTRIBUTING.md](https://gitlab.com/_thmsdmcrt/looper/blob/master/CONTRIBUTING.md) file.
## Licence
Please, see [LICENCE](https://gitlab.com/_thmsdmcrt/looper/blob/master/LICENSE) file.

[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat-square
[standard-url]: https://github.com/feross/standard
[stability-image]: https://img.shields.io/badge/stability-stable-brightgreen.svg?style=flat-square
[stability-url]: https://nodejs.org/api/documentation.html#documentation_stability_index
[npm-image]: https://img.shields.io/npm/v/@thmsdmcrt_/looper.svg?style=flat-square
[npm-url]: https://www.npmjs.com/package/@thmsdmcrt_/looper