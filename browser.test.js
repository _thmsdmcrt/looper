'use strict'
import { Looper } from './index'

/** Create a new instance of Looper. */
const looper = new Looper()

/** Create a method to call in looper. */
function doSomething () {
  console.log('looper is running...')
}

/** Add doSomething() to the looper instance. Save it to a variable to remove it later. */
const myMethodLooper = looper.add(doSomething) // { remove : function() {} }
/** Start the looper */
looper.start()
/** Alternatively, you can start the looper for a defined time: e.g 2000 milliseconds */
// looper.start(2000, {
//   onStart () {
//     console.log('start')
//   },
//   onUpdate (progress) {
//     console.log(progress)
//   },
//   onStop () {
//     console.log('stop')
//   }
// })

/** Get the looper status. */
const isLooperRunning = looper.isRunning() // true

// window.setTimeout(_ => {
//   /** Stop the looper instance. Here with a setTimeout */
//   looper.stop()

//   /** Remove the doSomething() from loop call stack. */
//   myMethodLooper.remove() // looper.actions.length = 0

//   /** Clear the looper instance. */
//   looper.dispose()
// }, 1000)
